package org.jaoed.target;

import org.pcap4j.packet.Packet;

public interface TargetResponse {
    public void sendResponse();
}
